#!/bin/sh

# Download and extract cmake source code
curl -L https://github.com/Kitware/CMake/archive/refs/tags/v3.27.7.tar.gz -o cmake.tar.gz
mkdir cmake-src && tar -xvf cmake.tar.gz --strip-components=1 -C cmake-src
rm cmake.tar.gz

# Get into cmake source code directory & compile it
cd cmake-src
./bootstrap --parallel=`nproc`
make -j `nproc`

# Install the compiled cmake and remove the source code dir
rm -rf cmake-src
make install