ARG ARCHITECTURE
FROM ${ARCHITECTURE}/ubuntu:20.04
ARG ARCHITECTURE

RUN echo "running regular task"
RUN echo "${ARCHITECTURE}"
RUN apt-get update

# install cross-compilers for armv7l and aarch64 for x64 image
RUN if [ "$ARCHITECTURE" = "amd64" ]; then \ 
        apt-get install -y -q gcc-arm-linux-gnueabi g++-arm-linux-gnueabi binutils-arm-linux-gnueabi; \
        apt-get install -y -q gcc-aarch64-linux-gnu g++-aarch64-linux-gnu binutils-aarch64-linux-gnu; \
    fi