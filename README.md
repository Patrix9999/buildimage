# Official Gothic 2 Online build image
Build image created for compilation of Gothic 2 Online. Includes CMake v3.27 and GCC for i686, arm32v7 and aarch64.

## Compilation for x64
To compile binary for 64-bit, use this compilers:
```bash
i686-linux-gnu-gcc
i686-linux-gnu-g++
```

## Compilation for ARM
To compile binary for 32-bit arm, use this compilers:
```bash
arm-linux-gnueabi-gcc
arm-linux-gnueabi-g++
```

To compile binary for 64-bit arm, use this compilers:
```bash
aarch64-linux-gnu-gcc
aarch64-linux-gnu-g++
```
